const express = require('express')
const path = require('path')
const adminControllers = require('../controllers/admin')
const router = express.Router();
const products = [];

router.get("/add-product",adminControllers.getAddProducts);
router.get("/products",adminControllers.getProducts)
router.post("/add-product",adminControllers.postAddProducts)
router.get('/edit-product/:productId',adminControllers.getEditProducts)
router.post('/edit-product',adminControllers.postEditProducts)
router.post('/delete-product',adminControllers.deleteProducts)


exports.routes = router;
exports.products=products;
