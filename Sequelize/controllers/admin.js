const Products = require("../models/products");
exports.getAddProducts = (req, res, next) => {
  res.render("admin/edit-product", {
    pageTitle: "add-products",
    path: "/admin/add-product",
    editing:false
  });
};

exports.getEditProducts = (req, res, next) => {
    const editMode = true;
    if(!editMode){
         return res.redirect('/')
    }
    const prodId = req.params.productId;
    req.user[0].getProducts({where :{id:prodId} })
    .then((products=>{
      if(!products){
        res.redirect("/edit-product")
    }
   
    res.render('admin/edit-product',{
        path : '/admin/edit-product',
        pageTitle : 'edit products',
        editing :editMode,
        product:products[0]
    })
    }))
    .catch(err=>{
      console.log(err)
    })
   
 
};
exports.postEditProducts = (req, res, next) => {
  const prodId = req.body.prodId;
  const updatedTitle = req.body.title;
  const updatedPrice = req.body.price;
  const updatedImageUrl = req.body.imageUrl;
  const updatedDesc = req.body.description;
    Products.update({ 
      title:updatedTitle,
      imageUrl:updatedImageUrl,
      price:updatedPrice,
      description:updatedDesc
     }, {
      where: {
        id: prodId
      }
    })
    .then(result => {
      console.log('UPDATED PRODUCT!');
      res.redirect('/admin/products');
    })
    .catch(err => console.log(err));
};

exports.postAddProducts = (req, res, next) => {
    const title =req.body.title;
    const imageUrl=req.body.imageUrl;
    const price=req.body.price;
   
    const description=req.body.description;
    
    
    Products.create({
      title:title,
      price:price,
      imageUrl:imageUrl,
      description:description,
      userId:req.user[0].id
    }).then((result)=>{
     
      res.redirect('/admin/products')
    })
    .catch((error)=>{
      console.log(error)
    })
 

  
};
exports.getProducts = (req, res, next) => {
  req.user[0].getProducts()
  .then((products=>{
    res.render("admin/products", {
      pageTitle: "admin products",
      prods: products,
      path: "/admin/products",
    });
  }))
  .catch((err)=>{
    console.log(err)
  })
};
exports.deleteProducts=(req,res,next)=>{
  const prodId = req.body.prodId;
  Products.destroy({
    where:{
      id:prodId
    }
  })
  .then(result=>{
    res.redirect('/admin/products')
  })
  .catch(err=>{
    console.log(err)
  })
}