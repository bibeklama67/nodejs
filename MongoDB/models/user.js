const getDb = require("../util/database").getDb;
const mongodb = require("mongodb");
class User {
  constructor(username, email, cart, id) {
    this.username = username;
    this.email = email;
    this.cart = cart; //{items:[]}
    this.id = id;
  }
  save() {
    const db = getDb();
    return db
      .collection("users")
      .insertOne(this)
      .then(() => {
        console.log("user created");
      })
      .catch((err) => {
        console.log(err);
      });
  }
  addToCart(product) {
    // const updatedCart = { items: [{productId: new mongodb.ObjectID(product._id), quantity: 1 }] };
    // const db = getDb();
    // return db
    //   .collection("users")
    //   .updateOne(
    //     { _id: new mongodb.ObjectID(this.id) },
    //     { $set: { cart: updatedCart } }
    //   );
    const cartProductIndex = this.cart.items.findIndex((cp) => {
      return cp.productId.toString() === product._id.toString();
    });

    let newQuantity;
    const updatedCartItem = [...this.cart.items];

    if (cartProductIndex >= 0) {
      newQuantity = this.cart.items[cartProductIndex].quantity + 1;
      updatedCartItem[cartProductIndex].quantity = newQuantity;
    } else {
      updatedCartItem.push({
        productId: new mongodb.ObjectID(product._id),
        quantity: 1,
      });
    }
    const updatedCart = { items: updatedCartItem };
    const db = getDb();
    return db
      .collection("users")
      .updateOne(
        { _id: new mongodb.ObjectID(this.id) },
        { $set: { cart: updatedCart } }
      );
  }
  static findById(userId) {
    const db = getDb();
    return db
      .collection("users")
      .findOne({ _id: new mongodb.ObjectID(userId) });
  }
  getCart() {
    const db = getDb();

    const productIds = this.cart.items.map((i) => {
      return i.productId;
    });
    return db
      .collection("products")
      .find({ _id: { $in: productIds } })
      .toArray()
      .then((products) => {
        return products.map((p) => {
          return {
            ...p,
            quantity: this.cart.items.find((i) => {
              return i.productId.toString() === p._id.toString();
            }).quantity,
          };
        });
      });
  }
  deleteItemFromCart(prodId) {
    const updatedCartItem = this.cart.items.filter((item) => {
      return item.productId.toString() !== prodId.toString();
    });

    const db = getDb();
    return db
      .collection("users")
      .updateOne(
        { _id: new mongodb.ObjectID(this.id) },
        { $set: { cart: { items: updatedCartItem } } }
      );
  }
  addOrder() {
    const db = getDb();
    return this.getCart()
      .then((products) => {
        const order = {
          items: products,
          users: {
            _id: new mongodb.ObjectID(this.id),
            username: this.username,
            email: this.email,
          },
        };
        return db.collection("orders").insertOne(order);
      })

      .then((result) => {
        this.cart = { items: [] };
        return db
          .collection("users")
          .updateOne(
            { _id: new mongodb.ObjectID(this.id) },
            { $set: { cart: { items: [] } } }
          );
      });
  }
  getOrders() {
    const db = getDb();
    return db
      .collection("orders")
      .find({"users._id": new mongodb.ObjectID(this.id) })
      .toArray();
  }
}
module.exports = User;
