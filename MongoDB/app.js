const express = require("express");
const app = express();
const path = require("path");
const User = require('./models/user')

const error = require("./controllers/error");

const adminData = require("./routes/admin");
const shopRouter = require("./routes/shop");

// // const sequelize = require("./util/database");

const bodyParser = require("body-parser");

app.set("view engine", "ejs");
app.set("views", "views");
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, "public")));

app.use((req,res,next)=>{
  User.findById("5ea2dd85ebd15229e03edf9d")
  .then(user=>{
    req.user = new User(user.username,user.email,user.cart,user._id)
    next();
  })
  .catch(err=>{console.log(err)})
  
})

app.use("/admin", adminData.routes);
app.use(shopRouter);
// app.use(error.get404);
// //Relations deko
// Product.belongsTo(User, { constraints: true, onDelete: "CASCADE" });
// User.hasMany(Product);
// User.hasOne(Cart);
// Cart.belongsTo(User);
// Cart.belongsToMany(Product,{through:CartItem})
// Product.belongsToMany(Cart,{through:CartItem})
// Order.belongsTo(User);
// User.hasMany(Order);
// Order.belongsToMany(Product,{through:OrderItem});




// sequelize
//   .sync()
//   .then((result) => {
//     return User.findAll({ where: { id: 1 } });
//     // console.log(result)
//   })
//   .then((user) => {
//     if (user.length == 0) {
//       return User.create({ name: "Bibek", email: "bibeklama67@gmail.com" });
//     }
//     return user;
//   })
//   .then((user)=>{
//     return user[0].createCart();
//   })
//   .then(cart => {
//     app.listen(3000);
//   })
//   .catch((err) => {
//     console.log(err);
//   });
const mongoConnect = require('./util/database').mongodbConnect
mongoConnect(()=>{
 
  app.listen(3000);
})