const mongodb = require("mongodb");
const con = mongodb.MongoClient;
let _db;
const mongodbConnect = (callback) => {
  con
    .connect(
      "mongodb+srv://bibeklama:kathmandunepal@cluster0-vkztp.mongodb.net/test?retryWrites=true&w=majority"
    )
    .then((client) => {
      console.log("connected")
      
      _db=client.db();
      callback();
    })
    .catch((err) => {
      console.log(err);
      throw err
    });
};
const getDb = ()=>{
    if(_db){
        return _db;
    }
    throw 'No database found'
};
exports.mongodbConnect = mongodbConnect;
exports.getDb = getDb;

