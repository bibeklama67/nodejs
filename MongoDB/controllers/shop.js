const Products = require("../models/products");
const Cart = require("../models/cart");
exports.getProducts = (req, res, next) => {
  Products.fetchAll()
    .then((products) => {
      res.render("shop/products-list", {
        prods: products,
        pageTitle: "product list",
        path: "/product-list",
      });
    })
    .catch((err) => {
      console.log(err);
    });
};
exports.getProduct = (req, res, next) => {
  // const prodId = req.params.productId;
  // Products.findById(prodId)
  // .then((products)=>{
  //   res.render('shop/product-detail',{
  //     pageTitle: 'details',
  //     path : '/details',
  //     product : products
  //   })

  // })

  const prodId = req.params.productId;
  Products.findById(prodId).then((products) => {
    res.render("shop/product-detail", {
      pageTitle: "details",
      path: "/details",
      product: products,
    });
  });
};
exports.getIndex = (req, res, next) => {
  Products.fetchAll()
    .then((products) => {
      res.render("shop/index", {
        pageTitle: "index",
        path: "/",
        prods: products,
      });
    })
    .catch((err) => {
      console.log(err);
    });
};

exports.getCart = (req, res, next) => {
  req.user
    .getCart()
    .then((products) => {
      res.render("shop/cart", {
        path: "/cart",
        pageTitle: "cartho",
        products: products,
      });
    })
    .catch((err) => {
      console.log(err);
    });
};
// Cart.getCart((cart) => {
//   Products.fetchAll((products) => {
//     const cartProducts = [];
//     for (product of products) {
//       const cartProductData = cart.products.find(
//         (prod) => prod.id === product.id
//       );
//       if (cartProductData) {
//         cartProducts.push({ productData: product, qty: cartProductData.qty });
//       }
//     }

//     res.render("shop/cart", {
//       path: "/cart",
//       pageTitle: "cartho",
//       products: cartProducts,
//     });
//   });
// });

exports.postCart = (req, res, next) => {
  const prodId = req.body.prodId;
  Products.findById(prodId)
    .then((product) => {
      return req.user.addToCart(product);
    })
    .then((result) => {
      res.redirect('/cart')
    });
  //   let fetchedCart;
  //   let newQuantity=1;
  //   req.user[0].getCart()
  //   .then(cart=>{
  //     fetchedCart = cart;
  //     return cart.getProducts({where:{id:prodId}})
  //   })
  //   .then(products=>{
  //     let product;
  //     if(products.length>0){
  //       product =products[0]
  //     }

  //     if(product){

  //       const oldQuantity = product.cartItem.quantity;

  //       newQuantity = oldQuantity +1;
  //       return product
  //     }
  //     return Products.findAll({where:{id:prodId}})

  //     })
  //     .then(product=>{
  //       return fetchedCart.addProduct(product,{
  //         through:{quantity:newQuantity}
  //       })
  //       .catch(err=>{console.log(err)})
  //     .then(()=>{
  //     res.redirect('/cart')
  //     })
  //   })
  //   .catch(err=>{console.log(err)})
  // };
};
exports.postCartDeleteProduct = (req, res, next) => {
  const prodId = req.body.prodId;
  req.user.deleteItemFromCart(prodId)
    
    .then((result) => {
      res.redirect("/cart");
    })
    .catch((err) => {
      console.log(err);
    });
};
exports.getOrder = (req, res, next) => {
  req.user.getOrders()
    .then((orders) => {
      console.log(orders)
      res.render("shop/orders", {
        path: "/orders",
        pageTitle: "order",
        orders: orders,
      });
    })
    .catch((err) => {
      console.log(err);
    });
};
exports.getCheckout = (req, res, next) => {
  res.render("shop/cart", {
    path: "/checkout",
    pageTitle: "checkout",
  });
};

exports.postOrder = (req, res, next) => {
  let fetchedCart;
  req.user.addOrder()
    
    .then((result) => {
      res.redirect("/orders");
    })
    .catch((err) => {
      console.log(err);
    });
};
