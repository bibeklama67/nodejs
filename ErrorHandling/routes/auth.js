const express = require("express");
const { check, body } = require("express-validator/check");
const authController = require("../controllers/auth");
const User = require('../models/user')
const router = express.Router();

router.get("/login", authController.getLogin);

router.get("/signup", authController.getSignup);

router.post("/login",
[
  body('email').isEmail().withMessage('Enter valid email'),
  body("password", "ramro password halnus")
  .isLength({ min: 5 })
  .isAlphanumeric()
]
,authController.postLogin);

router.post(
  "/signup",
  [
    check("email")
      .isEmail().normalizeEmail()
      .withMessage("melne email halum na metra ")
      .custom((value, { req }) => {
        //   if(value === 'bibek@bibek.com'){
        //       throw new Error('This email is forbidden')
        //   }
        //   return true
        return User.findOne({ email: value }).then((userDoc) => {
          if (userDoc) {
            return Promise.reject("Yo email tw pailai use garesakyo ne sathee");
          }
        });
      }),
    body("password", "ramro password halnus")
      .isLength({ min: 5 })
      .isAlphanumeric().trim(),
    body("confirmPassword").custom((value, { req }) => {
      if (value !== req.body.password) {
        throw new Error("Password same halna parxa ne");
      }
      return true;
    }).trim(),
  ],
  authController.postSignup
);

router.post("/logout", authController.postLogout);
router.get("/reset", authController.getReset);
router.post("/reset", authController.postReset);
router.get("/reset/:token", authController.newPassword);
router.post("/newPassword", authController.postNewPassword);

module.exports = router;
