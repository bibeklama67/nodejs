const Products = require("../models/products");
exports.getAddProducts = (req, res, next) => {
  res.render("admin/edit-product", {
    pageTitle: "add-products",
    path: "/admin/add-product",
    editing:false
  });
};

exports.getEditProducts = (req, res, next) => {
    const editMode = true;
    if(!editMode){
         return res.redirect('/')
    }
    const prodId = req.params.productId;
    Products.findById(prodId,(product)=>{
        if(!product){
            res.redirect("/edit-product")
        }
        res.render('admin/edit-product',{
            path : '/admin/edit-product',
            pageTitle : 'edit products',
            editing :editMode,
            product:product
        })
    })
   
 
};
exports.postEditProducts=(req,res,next)=>{
  const prodId = req.body.prodId;
  const title = req.body.title;
  const imageUrl = req.body.imageUrl;
  const price = req.body.price;
  const description = req.body.description;
  const updatedProduct = new Products(prodId,title,imageUrl,price,description);
  updatedProduct.save();
  
  res.redirect('/')
};

exports.postAddProducts = (req, res, next) => {
  const products = new Products(
    null,
    req.body.title,
    req.body.imageUrl,
    req.body.price,
    req.body.description
  );
  products.save();

  res.redirect("/");
 
};
exports.getProducts = (req, res, next) => {
  Products.fetchAll((products) => {
    res.render("admin/products", {
      pageTitle: "admin products",
      prods: products,
      path: "/admin/products",
    });
  });
};
exports.deleteProducts=(req,res,next)=>{
  const id = req.body.prodId;
  
  Products.deletePostById(id);
  res.redirect('/')
}