const Products = require("../models/products");
const Cart = require("../models/cart");
exports.getProducts = (req, res, next) => {
  Products.fetchAll((products) => {
    res.render("shop/products-list", {
      pageTitle: "all products",
      prods: products,
      path: "/products",
    });
  });
};
exports.getProduct = (req, res, next) => {
  const prodId = req.params.productId;

  Products.findById(prodId, (product) => {
    res.render("shop/product-detail", {
      path: "/details",
      pageTitle: prodId,
      product: product,
    });
  });
};
exports.getIndex = (req, res, next) => {
  Products.fetchAll((products) => {
    res.render("shop/index", {
      pageTitle: "shop",
      prods: products,
      path: "/",
    });
  });
};
exports.getCart = (req, res, next) => {
  Cart.getCart(cart =>{
    
    Products.fetchAll(products=>{
      
      const cartProducts =[];
      for(product of products){
       
        
        const cartProductData = cart.products.find(
          prod => prod.id === product.id
        );
        if(cartProductData){
          cartProducts.push({productData:product, qty:cartProductData.qty})
        }
      }
    
      res.render("shop/cart", {
        path: "/cart",
        pageTitle: "cartho",
        products : cartProducts
      });
    })
  })
  
};
exports.postCart = (req, res, next) => {
  const prodId = req.body.prodId;
  Products.findById(prodId, (products) => {
    Cart.addProduct(prodId, products.price);
  });
  res.redirect("/");
};

exports.postCartDeleteProduct=(req,res,next)=>{
  const prodId = req.body.prodId;
  
  Products.findById(prodId,product=>{
    Cart.deleteProduct(prodId,product.price);
    res.redirect('/cart')
  })
}
exports.getOrder = (req, res, next) => {
  res.render("shop/orders", {
    path: "/orders",
    pageTitle: "order",
  });
};
exports.getCheckout = (req, res, next) => {
  res.render("shop/cart", {
    path: "/checkout",
    pageTitle: "checkout",
  });
};
