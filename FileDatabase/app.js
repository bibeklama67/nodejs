const express = require('express');
const path = require('path');

const error = require('./controllers/error')

const adminData = require('./routes/admin');
const shopRouter = require('./routes/shop');
const bodyParser = require('body-parser');
const app = express();
app.set('view engine' , 'ejs');
app.set('views','views')
app.use(bodyParser.urlencoded({extended:false}))
app.use(express.static(path.join(__dirname,'public')))
app.use('/admin',adminData.routes);
app.use(shopRouter);
app.use(error.get404);
app.listen(3000);