const path = require("path");
const session = require("express-session");
const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const MongoDbStore = require('connect-mongodb-session')(session);

const errorController = require("./controllers/error");
const User = require("./models/user");
const store = new MongoDbStore({
  uri:"mongodb+srv://bibeklama:kathmandunepal@cluster0-vkztp.mongodb.net/bibek",
  collection:'session'
})
const app = express();

app.set("view engine", "ejs");
app.set("views", "views");

const adminRoutes = require("./routes/admin");
const shopRoutes = require("./routes/shop");
const authRoutes = require("./routes/auth");
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, "public")));

//session init
app.use(
  session({
    secret: "bibek lama",
    resave: false,
    saveUninitialized: false,
    store:store
  })
);
app.use((req,res,next)=>{
  if(!req.session.user){
    return next();
  }
  User.findById(req.session.user._id)
  .then(user=>{
    req.user = user;
    console.log(req.user)
    next();
  })
  .catch(err=>{console.log(err)})
});


app.use("/admin", adminRoutes);
app.use(shopRoutes);
app.use(authRoutes);
app.use(errorController.get404);

mongoose
  .connect(
    "mongodb+srv://bibeklama:kathmandunepal@cluster0-vkztp.mongodb.net/bibek?retryWrites=true&w=majority"
  )
  .then((result) => {
    User.findOne().then((user) => {
      if (!user) {
        const user = new User({
          name: "bibek",
          email: "bibeklama67@gmail.com",
          cart: {
            items: [],
          },
        });
        user.save();
      }
    });
    app.listen(3000);
  })
  .catch((err) => {
    console.log(err);
  });
