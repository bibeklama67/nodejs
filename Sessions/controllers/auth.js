const User = require('../models/user')
exports.getLogin = (req, res, next) => {

const isLoggedIn=(req.session.isLoggedIn)
  res.render("auth/login", {
    path: "/login",
    pageTitle: "login"
    ,isAuthenticated : isLoggedIn
    
  });
};
exports.postLogin=(req,res,next)=>{
 User.findById('5ea6f87c2ce8ad2690ae0217')
 .then(user=>{
   req.session.isLoggedIn = true;
   req.session.user = user;
   res.redirect('/')
 })
}
exports.postLogout=(req,res,next)=>{
  req.session.destroy(err=>{
    console.log(err)
    res.redirect('/')
  })
  
}
