const express = require("express");
const app = express();
const path = require("path");
const User = require("./models/user");

const error = require("./controllers/error");

const adminData = require("./routes/admin");
const shopRouter = require("./routes/shop");

const mongoose = require("mongoose");

const bodyParser = require("body-parser");

app.set("view engine", "ejs");
app.set("views", "views");
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, "public")));

app.use((req,res,next)=>{
  User.findById("5ea59e67fd5f01277ca9c6e8")
  .then(user=>{
    req.user =user
    next();
  })
  .catch(err=>{console.log(err)})

})

app.use("/admin", adminData.routes);
app.use(shopRouter);
app.use(error.get404);
// //Relations deko
// Product.belongsTo(User, { constraints: true, onDelete: "CASCADE" });
// User.hasMany(Product);
// User.hasOne(Cart);
// Cart.belongsTo(User);
// Cart.belongsToMany(Product,{through:CartItem})
// Product.belongsToMany(Cart,{through:CartItem})
// Order.belongsTo(User);
// User.hasMany(Order);
// Order.belongsToMany(Product,{through:OrderItem});

// sequelize
//   .sync()
//   .then((result) => {
//     return User.findAll({ where: { id: 1 } });
//     // console.log(result)
//   })
//   .then((user) => {
//     if (user.length == 0) {
//       return User.create({ name: "Bibek", email: "bibeklama67@gmail.com" });
//     }
//     return user;
//   })
//   .then((user)=>{
//     return user[0].createCart();
//   })
//   .then(cart => {
//     app.listen(3000);
//   })
//   .catch((err) => {
//     console.log(err);
//   });
mongoose
  .connect(
    "mongodb+srv://bibeklama:kathmandunepal@cluster0-vkztp.mongodb.net/shops?retryWrites=true&w=majority"
  )
  .then(() => {
    console.log("connected via mongoose");
    User.findOne().then(user=>{
      if(!user){
        const user = new User({
          username : 'bibeklama',
          email:'bibeklama67@gmail.com'
        })
        user.save();
      }
    })
    app.listen(3000);
  })
  .catch((err) => {
    console.log(err);
  });
