const Product = require("../models/products");
exports.getAddProducts = (req, res, next) => {
  res.render("admin/edit-product", {
    pageTitle: "add-products",
    path: "/admin/add-product",
    editing: false,
  });
};

exports.getEditProducts = (req, res, next) => {
  const editMode = true;
  if (!editMode) {
    return res.redirect("/");
  }
  const prodId = req.params.productId;
  Product.findById(prodId)
    .then((products) => {
      if (!products) {
        res.redirect("/edit-product");
      }

      res.render("admin/edit-product", {
        path: "/admin/edit-product",
        pageTitle: "edit products",
        editing: editMode,
        product: products,
      });
    })
    .catch((err) => {
      console.log(err);
    });
};
exports.postEditProducts = (req, res, next) => {
  const prodId = req.body.prodId;
  const updatedTitle = req.body.title;
  const updatedPrice = req.body.price;
  const updatedImageUrl = req.body.imageUrl;
  const updatedDesc = req.body.description;
  Product.findById(prodId)
  .then(product=>{
    product.title = updatedTitle;
    product.price = updatedPrice;
    product.description = updatedDesc;
    product.imageUrl= updatedImageUrl
    return product.save()
  })
 
    .then((result) => {
      console.log("UPDATED PRODUCT!");
      res.redirect("/admin/products");
    })
    .catch((err) => console.log(err));
};

exports.postAddProducts = (req, res, next) => {
  const title = req.body.title;
  const imageUrl = req.body.imageUrl;
  const price = req.body.price;
  const description = req.body.description;
  console.log("Aayo")
  const product = new Product({
    title:title,price:price,description:description,imageUrl:imageUrl,
    userId:req.user._id
  });

  product
    .save()
    .then((result) => {
      console.log("Created Product");
      res.redirect("/admin/products");
    })
    .catch((error) => {
      console.log(error);
    });
};
exports.getProducts = (req, res, next) => {
  Product.find()
    .then((products) => {
      res.render("admin/products", {
        pageTitle: "admin products",
        prods: products,
        path: "/admin/products",
      });
    })
    .catch((err) => {
      console.log(err);
    });
};
exports.deleteProducts = (req, res, next) => {
  const prodId = req.body.prodId;
  Product.findByIdAndRemove(prodId)
    .then((result) => {
      res.redirect("/admin/products");
    })
    .catch((err) => {
      console.log(err);
    });
};
