const Products = require("../models/products");
const Order = require("../models/order");
const Cart = require("../models/cart");
exports.getProducts = (req, res, next) => {
  Products.find()
    .then((products) => {
      res.render("shop/products-list", {
        prods: products,
        pageTitle: "product list",
        path: "/product-list",
      });
    })
    .catch((err) => {
      console.log(err);
    });
};
exports.getProduct = (req, res, next) => {
  // const prodId = req.params.productId;
  // Products.findById(prodId)
  // .then((products)=>{
  //   res.render('shop/product-detail',{
  //     pageTitle: 'details',
  //     path : '/details',
  //     product : products
  //   })

  // })

  const prodId = req.params.productId;
  Products.findById(prodId).then((products) => {
    res.render("shop/product-detail", {
      pageTitle: "details",
      path: "/details",
      product: products,
    });
  });
};
exports.getIndex = (req, res, next) => {
  Products.find()
    .then((products) => {
      res.render("shop/index", {
        pageTitle: "index",
        path: "/",
        prods: products,
      });
    })
    .catch((err) => {
      console.log(err);
    });
};

exports.getCart = (req, res, next) => {
  req.user
    .populate("cart.items.productIds")
    .execPopulate()
    .then((user) => {
      const products = user.cart.items;
      res.render("shop/cart", {
        path: "/cart",
        pageTitle: "cartho",
        products: products,
      });
    })
    .catch((err) => {
      console.log(err);
    });
};
// Cart.getCart((cart) => {
//   Products.fetchAll((products) => {
//     const cartProducts = [];
//     for (product of products) {
//       const cartProductData = cart.products.find(
//         (prod) => prod.id === product.id
//       );
//       if (cartProductData) {
//         cartProducts.push({ productData: product, qty: cartProductData.qty });
//       }
//     }

//     res.render("shop/cart", {
//       path: "/cart",
//       pageTitle: "cartho",
//       products: cartProducts,
//     });
//   });
// });

exports.postCart = (req, res, next) => {
  const prodId = req.body.prodId;
  Products.findById(prodId)
    .then((product) => {
      return req.user.addToCart(product);
    })
    .then((result) => {
      res.redirect("/cart");
    });
  //   let fetchedCart;
  //   let newQuantity=1;
  //   req.user[0].getCart()
  //   .then(cart=>{
  //     fetchedCart = cart;
  //     return cart.getProducts({where:{id:prodId}})
  //   })
  //   .then(products=>{
  //     let product;
  //     if(products.length>0){
  //       product =products[0]
  //     }

  //     if(product){

  //       const oldQuantity = product.cartItem.quantity;

  //       newQuantity = oldQuantity +1;
  //       return product
  //     }
  //     return Products.findAll({where:{id:prodId}})

  //     })
  //     .then(product=>{
  //       return fetchedCart.addProduct(product,{
  //         through:{quantity:newQuantity}
  //       })
  //       .catch(err=>{console.log(err)})
  //     .then(()=>{
  //     res.redirect('/cart')
  //     })
  //   })
  //   .catch(err=>{console.log(err)})
  // };
};
exports.postCartDeleteProduct = (req, res, next) => {
  const prodId = req.body.prodId;
  req.user
    .deleteItemFromCart(prodId)

    .then((result) => {
      res.redirect("/cart");
    })
    .catch((err) => {
      console.log(err);
    });
};

exports.getOrder = (req, res, next) => {
  Order.find({'user.userId':req.user._id})
    .then((orders) => {
      
      res.render("shop/orders", {
        path: "/orders",
        pageTitle: "order",
        orders: orders,
      });
    })
    .catch((err) => {
      console.log(err);
    });
};
exports.getCheckout = (req, res, next) => {
  res.render("shop/cart", {
    path: "/checkout",
    pageTitle: "checkout",
  });
};

exports.postOrder = (req, res, next) => {
  req.user
    .populate("cart.items.productIds")
    .execPopulate()
    .then((user) => {
      const products = user.cart.items.map((i) => {
        return { quantity: i.quantity, product:{...i.productIds._doc} };
      });
      const order = new Order({
        user: {
          name: req.user.username,
          userId: req.user,
        },
        products: products,
      });
      console.log("Order Created")
      return order.save();
    })
    .then(()=>{
      return req.user.clearCart();
    })
    .then((result) => {
      res.redirect("/orders");
    })
    .catch((err) => {
      console.log(err);
    });
};
