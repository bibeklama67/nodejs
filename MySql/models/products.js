const db= require('../util/database')
const Cart =require('./cart')


module.exports = class {
    constructor(id,title,imageUrl,price,description){
        this.id = id;
        this.title = title;
        this.imageUrl = imageUrl;
        this.description= description;
        this.price=price;
    }
    save(){
      return db.execute('INSERT INTO PRODUCTS (title,imageUrl,price,description) VALUES (?,?,?,?)',
     [this.title,this.imageUrl,this.price,this.description]
     );
      
    }
    static fetchAll(cb){
       return db.execute('SELECT * FROM products');
       
    } 
    static findById(id,cb){
     return db.execute("SELECT * FROM products WHERE products.id = ?",[id])
    }
}