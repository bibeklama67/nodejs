const Products = require("../models/products");
const Cart = require("../models/cart");
exports.getProducts = (req, res, next) => {
  Products.fetchAll()
  .then(([rows])=>{
    res.render('shop/products-list',{
      prods:rows,
      pageTitle:'product list',
      path:'/product-list'
    })
  })
};
exports.getProduct = (req, res, next) => {
  const prodId = req.params.productId;
  Products.findById(prodId)
  .then(([products])=>{
    res.render('shop/product-detail',{
      pageTitle: 'details',
      path : '/details',
      product : products[0]
    })

  })
 
};
exports.getIndex = (req, res, next) => {
  Products.fetchAll()
  .then(([rows])=>{
    res.render('shop/index',{
      prods:rows,
      pageTitle:'index',
      path:'/'
    })
  })
  }


exports.getCart = (req, res, next) => {
  Cart.getCart(cart =>{
    
    Products.fetchAll(products=>{
      
      const cartProducts =[];
      for(product of products){
       
        
        const cartProductData = cart.products.find(
          prod => prod.id === product.id
        );
        if(cartProductData){
          cartProducts.push({productData:product, qty:cartProductData.qty})
        }
      }
    
      res.render("shop/cart", {
        path: "/cart",
        pageTitle: "cartho",
        products : cartProducts
      });
    })
  })
  
};
exports.postCart = (req, res, next) => {
  const prodId = req.body.prodId;
  Products.findById(prodId, (products) => {
    Cart.addProduct(prodId, products.price);
  });
  res.redirect("/");
};

exports.postCartDeleteProduct=(req,res,next)=>{
  const prodId = req.body.prodId;
  
  Products.findById(prodId,product=>{
    Cart.deleteProduct(prodId,product.price);
    res.redirect('/cart')
  })
}
exports.getOrder = (req, res, next) => {
  res.render("shop/orders", {
    path: "/orders",
    pageTitle: "order",
  });
};
exports.getCheckout = (req, res, next) => {
  res.render("shop/cart", {
    path: "/checkout",
    pageTitle: "checkout",
  });
};
